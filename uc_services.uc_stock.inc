<?php

/**
 * @file
 * Integration of Services module for Ubercart
 */

/**
 * Get the stock level of a product. If stock tracing not active on this product, return 0.
 */
function uc_services_getstock($sku) {
  if (uc_stock_is_active($sku)) {
    return uc_stock_level($sku);
  }
  else {
    return 0;
  }
}

/**
 * Set the stock level for a product.
 */
function uc_services_setstock($sku, $level) {
  if (uc_stock_is_active($sku)) {
    uc_stock_set($sku, $level);
    
    if (uc_services_getstock($sku) == $leve) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Add stock items to the current stock of a product
 */
function uc_services_adjuststock($sku, $qty) {
  $currentstock = uc_services_getstock($sku);
  
  if (uc_stock_adjust($sku,$qty)) {
    if (uc_services_getstock($sku) == ($currentstock + $qty)) {
      return TRUE;  
    }
    else {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Check if this product has active stock tracing
 */
function uc_services_isstockactive($sku) {
  return uc_stock_is_active($sku);
}

/**
 * Enable stock tracing for a product
 */
function uc_services_setstockactive($sku) {
  if (!uc_stock_is_active($sku)) {
    if (db_result(db_query('SELECT nid FROM {uc_product_stock} WHERE sku = "%s"', $sku))) {
      db_query('UPDATE {uc_product_stock} SET active = %d WHERE sku = "%s"', 1, $sku);
    }
    else {
      $nid = db_result(db_query('SELECT nid FROM {uc_products} WHERE model = "%s"', $sku));
      db_query('INSERT INTO {uc_product_stock} (sku, nid, active) VALUES ("%s", %d, %d)', $sku, $nid, 1);
    }
  }
}